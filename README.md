# TCS Trommeschläger Genie IIIs: A Legacy Computer System

The TCS Trommeschläger Genie IIIs is a legacy computer system manufactured in Germany that was largely compatible with Tandy's TRS-80 systems. The Genie IIIs worked seamlessly with G-DOS, NEW-DOS or CP/M operating systems and was renowned for its high-resolution graphics. One of the key features of the Genie IIIs was its fast-paced Z80 processor, making it one of the fastest CP/M computers ever produced.

The Genie III's circuit board production was taken over by Siemens, making it even more widely available. Today, this legacy computer system continues to inspire modern-day emulation projects, such as the one we are undertaking to create versions for the popular SDLTRS emulator from Jens Guenther.

Our project aims to bring the Genie IIIs experience to a modern platform, allowing enthusiasts to enjoy this legacy system on their modern computers. By creating versions of Genie IIIs for the SDLTRS emulator, we are preserving the legacy of this iconic computer system for future generations to enjoy.

With our project, we hope to inspire other enthusiasts to join us in exploring the fascinating world of legacy computer systems and to celebrate the remarkable technological advancements of the past. Join us on this journey of rediscovery and relive the golden era of computing!

In this project use the https://github.com/Egbert-Azure/sdltrs emulator from Jens Guenther

![image](https://user-images.githubusercontent.com/55332675/219800153-87168b70-fd9c-40e2-b4cf-b99ee3ae718b.png)
